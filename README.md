# Kaka Farm's contributions to the world of GNU Guix.

## Contributions:

Tips, bug reports, suggestions, death threats, love letters, and
theories of global (or local) conspiracies go to
<mailto:yuval.langer@gmail.com>.  You can also bug me on
<https://libera.chat/> where I go by `cow_2001` or on the Matrix
federation where I go by `@falconstinker:matrix.org`

# Monetary support 💰:

You can buy me a unit of biomass on <https://buymeacoffee.com/kakafarm/>!

## Mirrors:

- Main: https://codeberg.org/kakafarm/guix-kakafarm-channel/
- https://kaka.farm/~stagit/guix-kakafarm-channel/log.html
