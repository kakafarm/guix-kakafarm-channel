;;; Kakafarm's Guix Channel
;;; Copyright © 2023-2024 Yuval Langer <yuval.langer@gmail.com>
;;;
;;; This file is part of Kakafarm's Guix Channel.
;;;
;;; Kakafarm's Guix Channel is free software; you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Kakafarm's Guix Channel is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Kakafarm's Guix Channel.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (kakafarm packages texinfo-documents)
  #:use-module (gnu packages base)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages perl-compression)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages texinfo)

  #:use-module (guix build-system copy)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:select (non-copyleft))
  #:use-module (guix packages))

(define-public (r7rs-small-texinfo target-format)
  (let ((commit "38a703976ea6353e32b52a5187dbdaf77fb2f050")
        (revision "3")
        (git-repository-url "https://codeberg.org/Zipheir/r7rs-small-texinfo/"))
    (package
      (name (string-append "r7rs-small-" target-format))
      (version (git-version "0.1.0" revision commit))
      (home-page git-repository-url)
      (source
       (origin
         (uri (git-reference
               (url git-repository-url)
               (commit commit)))
         (method git-fetch)
         (file-name (git-file-name name version))
         (sha256
          (base32 "1fr02fyhiwd364jkfy1n5w31pq3kx1rl5w634421g05702yb47x3"))))
      (native-inputs (list bash
                           coreutils
                           perl-archive-zip
                           texinfo
                           texlive-scheme-small
                           texlive-texinfo
                           sed
                           grep
                           perl
                           which
                           pkg-config
                           gettext-minimal))
      (inputs '())
      (build-system copy-build-system)
      (arguments
       (list
        #:install-plan #~`((,(string-append "r7rs-small."
                                            #$target-format) ,(string-append
                                                               "share/"
                                                               #$target-format
                                                               "/")))
        #:phases #~(modify-phases %standard-phases
                     (add-after 'patch-source-shebangs 'compile-the-files
                       (lambda _
                         (chdir "doc/r7rs-small")
                         (system* "makeinfo" "--help")
                         (system* "bash" "build.sh"
                                  #$target-format))))))
      (synopsis (string-append
                 "R7RS Small standard of the Scheme programming language in "
                 target-format " format"))
      (description (string-append
                    "Revised^7 Report of the Algorithmic Language Scheme adapted to Texinfo format. This package installs the  "
                    target-format " version of the document."))
      (license (non-copyleft "file://COPYING")))))

(define-public r7rs-small-epub
  (r7rs-small-texinfo "epub"))
(define-public r7rs-small-html
  (r7rs-small-texinfo "html"))
(define-public r7rs-small-info
  (r7rs-small-texinfo "info"))
(define-public r7rs-small-pdf
  (r7rs-small-texinfo "pdf"))
