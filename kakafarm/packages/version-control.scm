;;; Kakafarm's Guix Channel
;;; Copyright © 2023-2024 Yuval Langer <yuval.langer@gmail.com>
;;;
;;; This file is part of Kakafarm's Guix Channel.
;;;
;;; Kakafarm's Guix Channel is free software; you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Kakafarm's Guix Channel is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Kakafarm's Guix Channel.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (kakafarm packages version-control)
  #:use-module (gnu packages)
  #:use-module ((gnu packages version-control)
                #:prefix gnu::packages::version-control::)

  #:use-module (guix utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages))

(define-public libgit2-1.9
  (package
    (inherit gnu::packages::version-control::libgit2-1.7)
    (version "1.9.0")
    (source (origin
              (inherit (package-source gnu::packages::version-control::libgit2-1.7))
              (uri (git-reference
                    (url "https://github.com/libgit2/libgit2")
                    (commit (string-append "v" version))))
              (file-name (git-file-name "libgit2" version))
              (sha256
               (base32
                "06ajn5i5l1209z7x7jxcpw68ph0a6g3q67bmx0jm381rr8cb4zdz"))
              (patches
               (search-patches "libgit2-uninitialized-proxy-settings.patch"))
              (snippet
               '(begin
                  (for-each delete-file-recursively
                            '("deps/chromium-zlib"
                              "deps/llhttp"
                              "deps/ntlmclient"
                              "deps/pcre"
                              "deps/winhttp"
                              "deps/zlib"))))))
    (arguments (substitute-keyword-arguments (package-arguments gnu::packages::version-control::libgit2-1.7)
                 ((#:configure-flags flags #~(list))
                  #~(cons "-DEXPERIMENTAL_SHA256=ON"
                          (map (lambda (a-flag)
                                 (if (string= "-DUSE_HTTP_PARSER=system" a-flag)
                                     "-DUSE_HTTP_PARSER=http-parser"
                                     a-flag))
                               #$flags)))))))
