;;; Kakafarm's Guix Channel
;;; Copyright © 2023-2024 Yuval Langer <yuval.langer@gmail.com>
;;;
;;; This file is part of Kakafarm's Guix Channel.
;;;
;;; Kakafarm's Guix Channel is free software; you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Kakafarm's Guix Channel is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Kakafarm's Guix Channel.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (kakafarm packages rust-apps)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages crates-io))

(define-public rust-rusty-diceware-wordlists
  (package
    (name "rust-rusty-diceware-wordlists")
    (version "1.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "diceware_wordlists" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1sx1lbw5z0bvm9d1z0mkpz5d0k7b6f2wwga2il78jfnk7j2f6sw2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-serde" ,rust-serde-1))))
    (home-page "https://gitlab.com/yuvallangerontheroad/rusty-diceware")
    (synopsis "Wordlists for the command line tool Rusty Diceware")
    (description
     "Wordlists for the command line tool Rusty Diceware.

Featuring the Reinhold and Beale Diceware wordlists, the EFF wordlists, and MiniLock wordlist.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-rusty-diceware
  (package
    (name "rust-rusty-diceware")
    (version "0.5.8")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "diceware" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "082l4a444qq9scmnc25kqp11v02gv3yms1q8ysrhsirb04fvjy6y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-rusty-diceware-wordlists" ,rust-rusty-diceware-wordlists)
                       ("rust-getopts" ,rust-getopts-0.2)
                       ("rust-rand" ,rust-rand-0.8)
                       ("rust-rand-chacha" ,rust-rand-chacha-0.3))))
    (home-page "https://gitlab.com/yuvallangerontheroad/rusty-diceware")
    (synopsis "A command line diceware, with or without dice")
    (description
     "Commandline Diceware, with or without dice.

Inspired by the great passphrase generating solution Diceware invented
by Arnold G. Reinhold.

Featuring the Reinhold and Beale Diceware wordlists, the EFF wordlists, and MiniLock wordlist.")
    (license (list license:agpl3+))))
