;;; Kakafarm's Guix Channel
;;; Copyright © 2023-2024 Yuval Langer <yuval.langer@gmail.com>
;;;
;;; This file is part of Kakafarm's Guix Channel.
;;;
;;; Kakafarm's Guix Channel is free software; you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Kakafarm's Guix Channel is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Kakafarm's Guix Channel.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (kakafarm packages guile-xyz)
  #:use-module (ice-9 textual-ports)

  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages xorg)
  #:use-module ((gnu packages guile)
                #:prefix gnu:packages:guile:)
  #:use-module ((gnu packages guile-xyz)
                #:prefix gnu:packages:guile-xyz:)
  #:use-module (gnu packages image)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages speech)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages)

  #:use-module (guix build-system gnu)
  #:use-module (guix build-system guile)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix packages))

(define-public guile-bytevector-peg
  (let ((commit
         "c1078bca7905ece5388063a1cb0eacb47e429e33")
        (revision "1"))
    (package
      (name "guile-bytevector-peg")
      (version (git-version "0.4.0" revision commit))
      (source
       (origin
         (uri (git-reference
               (url "https://codeberg.org/kakafarm/temporary-sha1-guile-bytevector-peg/")
               (commit commit)))
         (method git-fetch)
         (file-name (git-file-name name version))
         (sha256
          (base32 "0myzrax4s39y31cirjsayvcvfj46djdxrpim4n2wmprbgh1hg03r"))))
      (inputs (list gnu:packages:guile:guile-3.0))
      (build-system guile-build-system)
      (home-page "https://codeberg.org/kakafarm/temporary-sha1-guile-bytevector-peg/")
      (synopsis "Bytevector PEGs for GNU Guile")
      (description "Bytevector PEGs for GNU Guile")
      (license license:agpl3+))))

(define-public guile-clipboard-speaker
  (let ((commit "36e87dee6f0ea51df62b1c52ff570f778ea18d95")
        (revision "2")
        (base32-string "1gz5fq8acw0nxr5nyarcrnxy75brjyr3ir5x6kj3mbdiijyqnagh")
        (git-repository-url
         "https://codeberg.org/kakafarm/guile-clipboard-speaker/"))
    (package
      (name "guile-clipboard-speaker")
      (version (git-version "1.0.2" revision commit))
      (source
       (origin
         (uri (git-reference
               (url git-repository-url)
               (commit commit)))
         (method git-fetch)
         (file-name (git-file-name name version))
         (sha256
          (base32 base32-string))))
      (native-inputs (list gnu:packages:guile:guile-3.0))
      (inputs (list bash
                    coreutils
                    espeak-ng
                    gnu:packages:guile-xyz:guile-config
                    gnu:packages:guile-xyz:guile-srfi-145
                    gnu:packages:guile-xyz:guile-srfi-180
                    gnu:packages:guile:guile-3.0
                    util-linux
                    xsel))
      (build-system guile-build-system)
      (arguments
       (list
        #:phases #~(modify-phases %standard-phases
                     (add-after 'install-documentation 'link-and-wrap-executable
                       (lambda _
                         (use-modules (ice-9 pretty-print))
                         (let* ((bin (string-append #$output "/bin"))
                                (script (string-append bin
                                                       "/clipboard-speaker"))
                                (site-version (target-guile-effective-version))
                                (scm (string-append "/share/guile/site/"
                                                    site-version))
                                (go (string-append "/lib/guile/" site-version
                                                   "/site-ccache"))
                                (guile-config #$(this-package-input
                                                 "guile-config"))
                                (guile-srfi-145 #$(this-package-input
                                                   "guile-srfi-145"))
                                (guile-srfi-180 #$(this-package-input
                                                   "guile-srfi-180")))
                           (mkdir-p bin)
                           (symlink (string-append #$output scm
                                                   "/clipboard-speaker.scm")
                                    script)
                           (wrap-program script
                             #:sh (which "bash")
                             (list "GUILE_LOAD_PATH"
                                   'prefix
                                   (list (string-append #$output scm)
                                         (string-append guile-config scm)
                                         (string-append guile-srfi-145 scm)
                                         (string-append guile-srfi-180 scm)))
                             (list "GUILE_LOAD_COMPILED_PATH"
                                   'prefix
                                   (list (string-append #$output go)
                                         (string-append guile-config go)
                                         (string-append guile-srfi-145 go)
                                         (string-append guile-srfi-180 go))))))))))
      (home-page git-repository-url)
      (synopsis
       "Accessibility tool that reads the contents of your clipboard buffer")
      (description
       "This package installs the clipboard-speaker executable.
An accessibility tool that reads the contents of the clipboard.  I can
read the current selection or the clipboard.  Select a bunch of text,
press a keybinding / shortcut you've set in the windows manager, and
clipboard-speaker would read that aloud.")
      (license license:agpl3+))))

(define-public guile-rsv
  (let ((commit "41b04c85eef31d4d51001c6d66e8fd339fcc614c")
        (revision "1")
        (base32-string "1w9jbkpmh13zrxkj915nm3l537smm0jsrdzrzcxylb6w59vqpw6l")
        (git-repository-url "https://codeberg.org/kakafarm/guile-rsv/"))
    (package
      (name "guile-rsv")
      (version (git-version "0.2.0" revision commit))
      (source
       (origin
         (uri (git-reference
               (url git-repository-url)
               (commit commit)))
         (method git-fetch)
         (file-name (git-file-name name version))
         (sha256
          (base32 base32-string))))
      (inputs (list gnu:packages:guile:guile-3.0 bash))
      (build-system guile-build-system)
      (arguments
       (list
        #:phases #~(modify-phases %standard-phases
                     (add-after 'install 'link-and-wrap-executable
                       (lambda _
                         (let* ((bin (string-append #$output "/bin"))
                                ;; bin directory for PATH.
                                (site-version (target-guile-effective-version))
                                (scm (string-append "/share/guile/site/"
                                                    site-version))
                                (go (string-append "/lib/guile/" site-version
                                                   "/site-ccache")))
                           (mkdir-p bin)
                           (for-each (lambda (command-name)
                                       (let ((source-script (string-append #$output
                                                             scm "/"
                                                             command-name
                                                             ".scm"))
                                             (target-command (string-append
                                                              bin "/"
                                                              command-name)))
                                         (symlink source-script target-command)
                                         (wrap-program target-command
                                           #:sh (which "bash")
                                           `("GUILE_LOAD_PATH" prefix
                                             (,(string-append #$output scm)))
                                           `("GUILE_LOAD_COMPILED_PATH" prefix
                                             (,(string-append #$output go))))))
                                     (list "scm2rsv" "rsv2scm"))))))))
      (home-page git-repository-url)
      (synopsis
       "Library for reading and writing Rows of String Values data format")
      (description
       "R7RS-small Scheme library for reading and writing RSV (Rows of String
Values) data format, a very simple binary format for storing tables of
strings.  It is a competitor for e.g. CSV (Comma Seperated Values),
and TSV (Tab Separated Values).  Its main benefit is that the strings
are represented as Unicode encoded as UTF-8, and the value and row
separators are byte values that are never used in UTF-8, so the
strings do not need any error prone escaping and thus can be written
and read verbatim.

Specified in https://github.com/Stenway/RSV-Specification and
demonstrated in https://www.youtube.com/watch?v=tb_70o6ohMA.")
      (license (list license:gpl3+ license:expat-0)))))

(define-public guile-srfi-133
  (let ((version "0.0.1")
        (revision "1")
        (commit "db81a114cd3e23375f024baec15482614ec90453")
        (git-repository-url
         "https://github.com/scheme-requests-for-implementation/srfi-133"))
    (package
      (name "guile-srfi-133")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url
                "https://github.com/scheme-requests-for-implementation/srfi-133")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0a7srl72291yah0aj6rwddhj041v2spximhknjj7hczlparsrm7f"))))
      (build-system guile-build-system)
      (arguments
       (list
        #:phases #~(modify-phases %standard-phases
                     (add-after 'unpack 'move-create-and-delete-files
                       (lambda _
                         (rename-file "vectors" "srfi")
                         (rename-file "srfi/vectors-test.scm"
                                      "srfi/srfi-test.scm")
                         (rename-file "srfi/vectors-impl.scm"
                                      "srfi/srfi-impl.scm")
                         (with-output-to-file "srfi/srfi-133.scm"
                           (lambda ()
                             (display
                              "(define-module (srfi srfi-133)
  #:replace (;; Constructors
             vector-copy

             ;; Mutators
             vector-fill! vector-copy!

             ;; Conversion
             vector->list list->vector)
  #:export (;; Constructors
            vector-unfold vector-unfold-right vector-reverse-copy
            vector-append vector-concatenate vector-append-subvectors

            ;; Predicates
            vector-empty? vector=

            ;; Iteration
            vector-fold vector-fold-right vector-map vector-map!
            vector-for-each vector-count vector-cumulate

            ;; Searching
            vector-index vector-index-right vector-skip vector-skip-right
            vector-binary-search vector-any vector-every vector-partition

            ;; Mutators
            vector-swap! vector-reverse!
            vector-reverse-copy! vector-unfold! vector-unfold-right!

            ;; Conversion
            reverse-vector->list reverse-list->vector
            vector->string string->vector))

(include \"srfi-impl.scm\")")))
                         (for-each (lambda (filename)
                                     (delete-file filename))
                                   '("tests/run.scm" "srfi/vectors.sld"
                                     "srfi/vectors.scm")))))))
      (native-inputs (list gnu:packages:guile:guile-3.0))
      (home-page git-repository-url)
      (synopsis "Vector Library (R7RS-compatible)")
      (description "A comprehensive library of vector operations.")
      (license license:expat))))

(define-public guile-srfi-232
  (let ((version "0.0.1")
        (revision "1")
        (commit "c3f580d220778cd71492aba4fdd0c7040968e705")
        (git-repository-url
         "https://github.com/scheme-requests-for-implementation/srfi-232"))
    (package
      (name "guile-srfi-232")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url
                "https://github.com/scheme-requests-for-implementation/srfi-232")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0lp4zcqjjj6hwfh3ix71wak1nffgg4npzsg7cdxfn9hf6iwf9xby"))))
      (build-system guile-build-system)
      (arguments
       (list
        #:phases #~(modify-phases %standard-phases
                     (add-after 'unpack 'move-and-delete-things
                       (lambda _
                         (let* ((srfi-directory (string-append #$output
                                                               "/srfi")))
                           (mkdir-p "srfi")
                           (with-output-to-file "srfi/srfi-232.scm"
                             (lambda ()
                               (display "(define-library (srfi srfi-232)
 (export curried define-curried)
 (import (only (guile) import)
         (scheme base))
 (include \"../srfi-232.scm\"))")))
                           (for-each (lambda (filename)
                                       (delete-file filename))
                                     '("test-body.scm" "test-chibi.scm"
                                       "test-srfi-64.scm"))))))))
      (native-inputs (list gnu:packages:guile:guile-3.0))
      (home-page git-repository-url)
      (synopsis "Flexible curried procedures")
      (description
       "Scheme lacks a flexible way to create and apply curried
procedures.  This SRFI describes curried, a variant of lambda that
creates true curried procedures which also behave just like ordinary
Scheme procedures.  They can be applied to their arguments one by one,
all at once, or anywhere in between, without any novel syntax.
curried also supports nullary and variadic procedures, and procedures
created with it have predictable behavior when applied to surplus
arguments.")
      (license license:expat))))

(define-public guile-srfi-235
  (let ((version "1.0.0")
        (revision "1")
        (commit "643a44aa9d6872962257995ecb0a31eb06a71d88"))
    (package
      (name "guile-srfi-235")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url
                "https://github.com/scheme-requests-for-implementation/srfi-235")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1slkcr67ad12ipkbhjdzjhbnsyvq5wi7cssvgv110fr2dy4rciwp"))))
      (build-system guile-build-system)
      (arguments
       (list
        #:phases #~(modify-phases %standard-phases
                     (add-after 'unpack 'move-create-and-delete-files
                       (lambda _
                         (substitute* "srfi/235.sld"
                           (("srfi 235")
                            "srfi srfi-235"))
                         (rename-file "srfi/235.sld" "srfi/srfi-235.scm"))))))
      (native-inputs (list gnu:packages:guile:guile-3.0))
      (home-page
       "https://github.com/scheme-requests-for-implementation/srfi-235")
      (synopsis "Combinators for Guile Scheme")
      (description
       "This SRFI contains various procedures that accept and return procedures, as
well as a few others, drawn from an earlier version of Chicken.
Common Lisp has a few of them too, and more come from the Standard
Prelude from Programming Praxis.  Using these procedures helps to keep
code terse and reduce the need for ad hoc lambdas.")
      (license license:expat))))

(define-public guile-srfi-253
  (let ((version "0.0.1")
        (revision "3")
        (commit "f72830bb9b8fd20b05145a7a0b78765ff9a571b0")
        (git-repository-url
         "https://github.com/scheme-requests-for-implementation/srfi-253"))
    (package
      (name "guile-srfi-253")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url
                "https://github.com/scheme-requests-for-implementation/srfi-253")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0ni137rar3w8wr329br82z2h2nq3qnmxfg8vkw5lzkg0vapa8z3w"))))
      (build-system guile-build-system)
      (arguments
       (list
        #:phases #~(modify-phases %standard-phases
                     (add-after 'unpack 'move-create-and-delete-files
                       (lambda _
                         (use-modules (ice-9 textual-ports))
                         (let* ((impl (call-with-input-file "srfi/impl.scm"
                                        get-string-all)))
                           (with-output-to-file "srfi/srfi-253.scm"
                             (lambda ()
                               (display "(define-module (srfi srfi-253)
  #:export (
   case-lambda-checked
   check-arg
   define-checked
   define-optionals-checked
   lambda-checked
   opt-lambda-checked
   values-checked
   ))
")
                               (display impl))))

                         (for-each (lambda (filename)
                                     (delete-file filename))
                                   '("existing-practice.scm" "srfi/253.sld"
                                     "srfi/253.sls" "srfi/impl.scm" "test.scm")))))))
      (native-inputs (list gnu:packages:guile:guile-3.0))
      (home-page git-repository-url)
      (synopsis "Data (Type-)Checking.")
      (description
       "Data validation and type checking (supposedly) make for more correct code. And faster code too, sometimes. And, in rare cases, code that's easier to follow than the un-checked one. Unfortunately, Scheme does not have many (type-)checking primitives out of the box. This SRFI provides some, with the aim of allowing more performant and correct code with minimum effort on the user side. Both (manual) argument checking/validation (check-arg) and return value(s) (values-checked) checking/coercion are provided. Syntax sugar like lambda-checked and define-checked is added on top.")
      (license license:expat))))

(define-public guile-websocket-next
  (let ((version "0.2")
        (revision "1")
        (commit "438d87675d9ef18695475685ff36ff75a5506466"))
    (package
      (inherit gnu:packages:guile-xyz:guile-websocket)
      (name "guile-websocket-next")
      (home-page "https://dthompson.us/projects/guile-websocket.html")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://git.dthompson.us/guile-websocket.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1qqwjhpqc615sxjnmx8jbpjqczfanfjvhrl1lxd3h738pnjr7fbq")))))))
