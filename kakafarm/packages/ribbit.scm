;;; Kakafarm's Guix Channel
;;; Copyright © 2023-2024 Yuval Langer <yuval.langer@gmail.com>
;;;
;;; This file is part of Kakafarm's Guix Channel.
;;;
;;; Kakafarm's Guix Channel is free software; you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Kakafarm's Guix Channel is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Kakafarm's Guix Channel.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (kakafarm packages ribbit)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages scheme)

  #:use-module (guix build-system copy)
  #:use-module (guix build-system trivial)
  #:use-module (guix modules)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  )

(define ribbit-commit "11dd04a9546f99c557b81fa0fd069c693f5eddb5")
(define git-repository-url "https://github.com/udem-dlteam/ribbit")
(define ribbit-base32-string "1kvfa6zi9jbjy2c5cqh5y84mcxd68rz77xzq3h59qr7lji3rjj47")

(define-public ribbit-rsc.exe
  (let ((revision "1"))
    (package
      (name "ribbit-rsc.exe")
      (version (git-version "0.0.0" revision ribbit-commit))
      (source
       (origin
         (uri (git-reference
               (url git-repository-url)
               (commit ribbit-commit)))
         (method git-fetch)
         (file-name (git-file-name name version))
         (sha256
          (base32 ribbit-base32-string))))
      (native-inputs (list
                      coreutils
                      bash-minimal
                      gcc-toolchain
                      gambit-c
                      ))
      ;; (inputs (list gcc-toolchain))
      (build-system copy-build-system)
      (arguments
       (list
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'unpack 'move-create-and-delete-files
              (lambda _
                (chdir "src")
                (system* "make" "rsc.exe")
                (chdir ".."))))
        #:install-plan
        #~(let ((docs (string-append "share/doc/"
                                     #$name
                                     "-"
                                     #$version
                                     "/"))
                (src (string-append "usr/src/"
                                    #$name
                                    "-"
                                    #$version
                                    "/"))
                )
            `(
              ("README.md" ,docs)
              ("CITATION.md" ,docs)
              ("AUTHORS" ,docs)
              ("LICENSE" ,docs)
              ("docs" ,docs)
              ("src/" ,src)
              ))
        ))
      (home-page git-repository-url)
      (synopsis
       "A Scheme runtime compiler")
      (description
       "A Scheme runtime compiler.")
      (license license:expat))))

(define-public ribbit-javascript-r4rs
  (let ((revision "1"))
    (package
      (name "ribbit-javascript-r4rs")
      (version (git-version "0.0.0" revision ribbit-commit))
      (source #f)
      (native-inputs (list
                      coreutils
                      bash-minimal
                      gcc-toolchain
                      ribbit-rsc.exe
                      ))
      ;; (inputs (list gcc-toolchain))
      (build-system trivial-build-system)
      (arguments
       (list
        #:builder
        (with-imported-modules (source-module-closure
                                '((guix build utils)))
          #~(let* ((ribbit-src-path (string-append #$ribbit-rsc.exe
                                                   "/usr/src/"
                                                   #$(package-name ribbit-rsc.exe)
                                                   "-"
                                                   #$(package-version ribbit-rsc.exe)))
                   (output-lib-path (string-append #$output
                                                   "/usr/lib/"
                                                   #$name
                                                   "-"
                                                   #$version))
                   (output-repl-target (string-append output-lib-path
                                                      "/repl.js")))
              (import (ice-9 ftw)
                      (guix build utils))
              (mkdir-p output-lib-path)
              (chdir ribbit-src-path)
              (system* "./rsc.exe"
                       "-t" "js"
                       "-l" "r4rs"
                       "lib/r4rs/repl.scm"
                       "-o" output-repl-target
                       )
              #t))))
      (home-page git-repository-url)
      (synopsis
       "A Javascript Ribbit Scheme runtime")
      (description
       "A Javascript Ribbit Scheme runtime.")
      (license license:expat))))
