;;; Kakafarm's Guix Channel
;;; Copyright © 2023-2024 Yuval Langer <yuval.langer@gmail.com>
;;;
;;; This file is part of Kakafarm's Guix Channel.
;;;
;;; Kakafarm's Guix Channel is free software; you can redistribute it
;;; and/or modify it under the terms of the GNU General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Kakafarm's Guix Channel is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Kakafarm's Guix Channel.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (kakafarm packages emacs-xyz)
  #:use-module (guix build-system emacs)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)

  #:use-module ((gnu packages emacs-xyz) #:prefix gnu:packages:emacs-xyz:)
  #:use-module (gnu packages speech)
  #:use-module (gnu packages)
  )

(define-public emacs-detubifier
  (let ((commit "2146cd76df48c8a9212486060e81c4654958c976")
        (revision "1")
        (git-repository-url
         "https://codeberg.org/kakafarm/emacs-detubifier"))
    (package
      (name "emacs-detubifier")
      (version (git-version "0.1.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url git-repository-url)
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1fv4hmr8zqnng241xkpsmyr63ggi9681rng1rm8bc7w8rlkzrwca"))))
      (build-system emacs-build-system)
      (home-page git-repository-url)
      (synopsis "Replace proprietary web frontends with free and privacy respecting web frontends")
      (description "Replace proprietary web frontends with free and privacy respecting web frontends.")
      (license license:agpl3+))))

(define-public emacs-discourse-mode
  (let ((commit "c5fde79990ab5d44786721f133b1c66764d6dc2b")
        (revision "1")
        (git-repository-url
         "https://codeberg.org/glenneth/discourse-mode"))
    (package
      (name "emacs-discourse-mode")
      (version (git-version "0.2.4" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url git-repository-url)
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "0j2f0vclj6zrmk1hxyrrhkcpk74vqvq42g9wjqrjw827yjh2wm65"))))
      (build-system emacs-build-system)
      (propagated-inputs (list gnu:packages:emacs-xyz:emacs-request
                               gnu:packages:emacs-xyz:emacs-compat
                               gnu:packages:emacs-xyz:emacs-markdown-mode))
      (home-page git-repository-url)
      (synopsis "Emacs package for interacting with Discourse forums")
      (description "Browse categories, view topics, read posts, and
participate in discussions directly from Emacs.")
      (license license:gpl3+))))

(define-public emacs-nano-tts-minor-mode
  (let ((commit "52a3ffa5833bdda79eb36e5048fe97c1b59eb9f2")
        (base32-string "15lzcd95zadd8wbzl5zdn72rad41mga2qqz34dc0sp8xgfwg3krh")
        (version "1.0.0")
        (revision "1")
        (git-repository-url
         "https://codeberg.org/kakafarm/emacs-nano-tts-minor-mode"))
    (package
      (name "emacs-nano-tts-minor-mode")
      (version (git-version version revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url git-repository-url)
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 base32-string))))
      (build-system emacs-build-system)
      (inputs (list espeak-ng))
      (home-page git-repository-url)
      (synopsis
       "Text-to-speech accessibility tool which reads aloud the active region")
      (description
       "A text-to-speech accessibility tool which reads aloud the active region.

Speaking is done using espeak-ng.")
      (license license:gpl3+))))

(define-public emacs-greader-mode
  (let ((commit "e163aec6109ba24ec543f087d9be7bf6b6efa389")
        (revision "1")
        (git-repository-url
         "https://gitlab.com/michelangelo-rodriguez/greader"))
    (package
      (name "emacs-greader-mode")
      (version (git-version "0.11.18" revision commit))
      (home-page git-repository-url)
      (source
       (origin
         (uri (git-reference
               (url git-repository-url)
               (commit commit)))
         (method git-fetch)
         (file-name (git-file-name name version))
         (sha256
          (base32 "054jlv1vrb7wbhr0w97xirjwp42mx0k07j7f0383jxsjn08qik5g"))))
      (build-system emacs-build-system)
      (arguments
       (list
        #:phases #~(modify-phases %standard-phases
                     (add-after 'unpack 'add-requires
                       (lambda _
                         (substitute* "greader-dict.el"
                           ((";;; Code:")
                            ";;; Code:\n(require 'greader)\n")))))))
      (inputs (list espeak-ng))
      (synopsis
       "Gnamù Reader - greader-mode, send buffer contents to a speech engine")
      (description
       "Greader is a module that allows you to send any emacs buffer to a TTS.
A text-to-speech like engine @code{espeak-ng} or @code{speech-dispatcher} are
already supported, plus limited bakend support native to macOS.  The
mode supports timer reading, automatic scrolling of buffers in modes
like @code{info-mode}, repeating reading of regions or the whole buffer,
includes a feature to facilitate the compilation of espeak-ng
pronunciations, and other features.")
      (license license:gpl3+))))

(define-public emacs-super-duper-yes-or-no
  (let ((commit "26eeb8655d20c10847437e985f021ab0772e76dd")
        (revision "1")
        (git-repository-url
         "https://codeberg.org/kakafarm/emacs-super-duper-yes-or-no"))
    (package
      (name "emacs-super-duper-yes-or-no")
      (version (git-version "1.0.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url git-repository-url)
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1ksvdac41fq9j6wk9myw6i0z1jp18lp2al95g9nidf6jimazdyxc"))))
      (build-system emacs-build-system)
      (home-page git-repository-url)
      (synopsis "Replace yes-or-no-p with something even more demanding")
      (description
       "Instead of replacing yes-or-no-p with y-or-n-p, replace it with
something even more mentally demanding.

Currently implemented:

- super-duper-yes-or-no-words-p.
- super-duper-yes-or-no-toggle-case-p.
- super-duper-yes-or-no-arithmetic-problem-p.

Consult the README.org file or the Emacs help of these functions for
more information.")
      (license license:gpl3+))))
